# gitlab_first

给套蜡笔测试

git 使用方法总结
===

---

* 克隆仓库 
`git clone 仓库地址`

* 克隆仓库中的指定分支
`git clone -b 分支名称 仓库地址`

* 查看当前分支(本地)
`git branch`

* 查看当前分支(本地+线上)
`git branch -a`

* 创建分支
`git branch 分支名`

* 删除分支(空分支)
`git branch -d 分支名`

* 强制删除分支(非空分支)
`git branch -D 分支名`

* 切换分支
`git checkout 分支名`

* 创建并切换到指定分支
`git checkout -b 分支名`

* 添加到仓库
`git add 文件名`

* 删除添加错误
`git rm -r --cached 文件名`

* 查看添加状态
`git status`

* 提交到本地仓库
`git commit -m '描述信息'`

* 推送到线上仓库
`git push origin 本地分支:线上分支(不存在就创建)`

* 将本地分支和线上分支同步
`git fetch`

* 将线上分支同步到本地分支
`git pull`

* 查看`push`历史版本 
`git log`

* 回退到某个版本(只是本地)
`git reset --hard 版本号`

* 将线上分支回退，接上一步
`git push origin 分支名 --force`

# 设置用户名
`git config [--global] user.name "[name]"`

# 设置邮箱
`git config [--global] user.email "[email address]"`

# 生成公钥
`ssh-keygen -t rsa -C "xxxxx@xxxxx.com"`

# 查看公钥
`cat ~/.ssh/id_rsa.pub`


---
/v1/user/change_child_amount 修改余额限额
```
参数：
* id: 账号ID
* amountLimit: 要修改的余额限额
```

v1/user/get_info_list 返回信息部列表

```
无参数
```

/v1/route/list 获取专线接口
```
参数：
* status: 可选参数
* userId: 总账号ID
* infoDeId: 要筛选信息部的ID
```

/v1/transport/list 获取运单接口
#####  method
```
GET, POST
```
```
参数
* status: 运单状态 （PROCESS, DONE）
* page: 页数
* limit: 每页数量
* infoDeId: 要筛选信息部的ID
```

15810541064

晋F79679
京Q98765
线上是服务端异常